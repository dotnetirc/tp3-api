﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP3_WS.Models.EntityFramework;
using TP3_WS.Models.Repository;

namespace TP3_WS.Models.DataManager
{
    public class CompteManager : IDataRepository<Compte>
    {
        readonly TP3Context tp3Context;

        public CompteManager(TP3Context context)
        {
            tp3Context = context;
        }

        public ActionResult<IEnumerable<Compte>> GetAll()
        {
            return tp3Context.Compte.ToList();
        }

        public ActionResult<Compte> GetById(int id)
        {
            return tp3Context.Compte.FirstOrDefault(e => e.CompteId == id);
        }

        public ActionResult<IEnumerable<Compte>> GetByString(string mail)
        {
            return new ActionResult<IEnumerable<Compte>>(tp3Context.Compte.Where(e => e.Mel.ToUpper() == mail.ToUpper()));
        }

        public void Add(Compte entity)
        {
            tp3Context.Compte.Add(entity);
            tp3Context.SaveChanges();
        }
        public void Update(Compte compte, Compte entity)
        {
            tp3Context.Entry(compte).State = EntityState.Modified;
            compte.CompteId = entity.CompteId;
            compte.Nom = entity.Nom;
            compte.Prenom = entity.Prenom;
            compte.Mel = entity.Mel;
            compte.Rue = entity.Rue;
            compte.CodePostal = entity.CodePostal;
            compte.Ville = entity.Ville;
            compte.Pays = entity.Pays;
            compte.Latitude = entity.Latitude;
            compte.Longitude = entity.Longitude;
            compte.Pwd = entity.Pwd;
            compte.TelPortable = entity.TelPortable;
            compte.FavorisCompte = entity.FavorisCompte;
            tp3Context.SaveChanges();
        }
        public void Delete(Compte compte)
        {
            tp3Context.Compte.Remove(compte);
            tp3Context.SaveChanges();
        }
    }
}

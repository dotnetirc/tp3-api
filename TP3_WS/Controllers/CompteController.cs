﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TP3_WS.Models.DataManager;
using TP3_WS.Models.EntityFramework;
using TP3_WS.Models.Repository;

namespace TP3_WS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompteController : ControllerBase
    {
        private readonly IDataRepository<Compte> _dataRepository;

        public CompteController(IDataRepository<Compte> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        // GET: api/Compte
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Compte>>> GetCompte(string email)
        {
            return _dataRepository.GetByString(email);
        }

        // GET: api/Compte/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Compte>> GetCompteById(int id)
        {
            var compte = _dataRepository.GetById(id);
            if (compte == null)
            {
                return NotFound();
            }
            return compte;
        }

        // PUT: api/Compte/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutCompte(int id, Compte compte)
        {
            if (id != compte.CompteId)
            {
                return BadRequest();
            }
            var compteToUpdate = _dataRepository.GetById(id);
            if (compteToUpdate == null)
            {
                return NotFound();
            }
            _dataRepository.Update(compteToUpdate.Value, compte);
            return NoContent();
        }

        // POST: api/Compte
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Compte>> PostCompte(Compte compte)
        {
            _dataRepository.Add(compte);

            return CreatedAtAction("GetCompteById", new { id = compte.CompteId }, compte);
        }

        // DELETE: api/Compte/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Compte>> DeleteCompte(int id)
        {
            var compte = _dataRepository.GetById(id);
            if (compte == null)
            {
                return NotFound();
            }
            _dataRepository.Delete(compte.Value);
            return compte;
        }

    }
}

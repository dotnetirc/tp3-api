using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TP3_WS.Controllers;
using TP3_WS.Models.DataManager;
using TP3_WS.Models.EntityFramework;

namespace FilmTest
{
    [TestClass]
    public class UnitTest1
    {
        private TP3Context tp3Context;
        private CompteController compteController;

        [TestInitialize]
        public void TestInitialize()
        {
            var builder = new DbContextOptionsBuilder<TP3Context>().UseNpgsql("Server=localhost;port=5432;Database=FilmRatingsDB; uid=postgres; password=postgres;");
            tp3Context = new TP3Context(builder.Options);
            CompteManager manager = new CompteManager(tp3Context);
            compteController = new CompteController(manager);
        }

        [TestMethod]
        public void PostCompte_ModelValidated_CreationOK()
        {
            // Arrange
            Random rnd = new Random();
            int chiffre = rnd.Next(1, 1000000000);
            // Le mail doit �tre unique donc 2 possibilit�s :
            // 1. on s'arrange pour que le mail soit unique en concat�nant un random ou un timestamp
            // 2. On supprime le compte apr�s l'avoir cr��. Dans ce cas, nous avons besoin d'appeler la m�thode DELETE du WS => la d�commenter
            Compte compteAtester = new Compte()
            {
                Nom = "MACHIN",
                Prenom = "Luc",
                TelPortable = "0606070809",
                Mel = "machin" + chiffre + "@gmail.com",
                Pwd = "Toto1234!",
                Rue = "Chemin de Bellevue",
                CodePostal = "74940",
                Ville = "Annecy-le-Vieux",
                Pays = "France",
                Latitude = null,
                Longitude = null
            };
            // Act
            var result = compteController.PostCompte(compteAtester).Result; // .Result pour appeler la m�thode async de mani�re synchrone, afin d'obtenir le r�sultat
            var result2 = compteController.GetCompte(compteAtester.Mel);
            var actionResult = result2.Result;
            // Assert
            Assert.IsInstanceOfType(actionResult.Value.First(), typeof(Compte), "Pas un compte");
            Compte compteRecupere = tp3Context.Compte.Where(c => c.Mel ==
                                                               compteAtester.Mel).FirstOrDefault();
            // On ne connait pas l'ID du compte envoy� car num�ro automatique.
            // Du coup, on r�cup�re l'ID de celui r�cup�r� et on compare ensuite les 2 comptes
            compteAtester.CompteId = compteRecupere.CompteId;
            Assert.AreEqual(compteRecupere, compteAtester, "Comptes pas identiques");
        }
    }
}
